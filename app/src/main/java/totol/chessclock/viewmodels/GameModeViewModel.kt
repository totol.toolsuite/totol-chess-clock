package totol.chessclock.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.GlobalScope.coroutineContext
import totol.chessclock.databases.AppDB
import totol.chessclock.entites.GameMode

class GameModeViewModel(application: Application) : AndroidViewModel(application) {
    var gameModeDao = AppDB.getDatabase(application).gameModeDao()
    private val scope = CoroutineScope(coroutineContext)

    fun insert(gameMode: GameMode) = viewModelScope.launch {
        gameModeDao.save(gameMode)
    }

    fun getOne(id: Int): LiveData<GameMode> {
        return gameModeDao.getById(id)
    }

    fun getAll(): LiveData<MutableList<GameMode>> {
        return gameModeDao.getAll()
    }

    fun nukeTable() = scope.launch(Dispatchers.IO) {
        gameModeDao.nukeTable()
    }

    fun deleteOne(gameMode: GameMode) = scope.launch(Dispatchers.IO) {
        gameModeDao.deleteOne(gameMode)
    }
}
