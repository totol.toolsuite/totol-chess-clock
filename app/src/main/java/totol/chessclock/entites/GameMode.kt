package totol.chessclock.entites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "game_mode")
data class GameMode(
        @PrimaryKey(autoGenerate = true)
        var id: Int?,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "black_time") var blackTime: Int,
        @ColumnInfo(name = "white_time") var whiteTime: Int,
        @ColumnInfo(name = "black_increment") var blackIncrement: Int,
        @ColumnInfo(name = "white_increment") var whiteIncrement: Int
) {
    constructor() : this(null,"", 0,0,0,0)
}
