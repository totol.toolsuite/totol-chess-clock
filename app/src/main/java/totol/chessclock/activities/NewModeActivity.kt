package totol.chessclock.activities

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_new_mode.*
import totol.chessclock.entites.GameMode
import totol.chessclock.R
import totol.chessclock.databinding.ActivityNewModeBinding
import totol.chessclock.viewmodels.GameModeViewModel

class NewModeActivity : AppCompatActivity() {
    var isIdentical : Boolean = false
    var gameMode = GameMode()
    var creationOk : Boolean = false
    private lateinit var gameModeViewModel: GameModeViewModel
    private lateinit var binding : ActivityNewModeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_mode)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_mode)
        setupBasicForm()
        setButtonsListeners()
        binding.newmode = gameMode
        gameModeViewModel = ViewModelProviders.of(this).get(GameModeViewModel::class.java)
    }

    private fun setupBasicForm() {
        gameMode.whiteTime = 5
        gameMode.whiteIncrement = 2
        gameMode.blackTime = 5
        gameMode.blackIncrement = 2
    }

    private fun setButtonsListeners() {
        activity_new_mode_pause.setOnClickListener {
            finish()
        }
        activity_new_mode_identical_yes.setOnClickListener {
            this.isIdentical = true
            activity_new_mode_identical_yes.background = ContextCompat.getDrawable(this, R.drawable.primary_round)
            activity_new_mode_identical_no.background = ContextCompat.getDrawable(this, R.drawable.disable_round)
            identicalHideOrShow()
        }
        activity_new_mode_identical_no.setOnClickListener {
            this.isIdentical = false
            activity_new_mode_identical_yes.background = ContextCompat.getDrawable(this, R.drawable.disable_round)
            activity_new_mode_identical_no.background = ContextCompat.getDrawable(this, R.drawable.primary_round)
            identicalHideOrShow()
        }
        activity_new_mode_white_limit_mine.setOnClickListener {
            if (gameMode.whiteTime > 1) {
                gameMode.whiteTime --
                binding.invalidateAll()
                checkAllowedTime(activity_new_mode_white_limit_mine, gameMode.whiteTime)

            }
        }
        activity_new_mode_white_limit_plus.setOnClickListener {
            if (gameMode.whiteTime < 1000) {
                gameMode.whiteTime ++
                binding.invalidateAll()
                checkAllowedTime(activity_new_mode_white_limit_mine, gameMode.whiteTime)
            }
        }
        activity_new_mode_white_increment_mine.setOnClickListener {
            if (gameMode.whiteIncrement > 0) {
                gameMode.whiteIncrement --
                binding.invalidateAll()
                checkAllowedIncrement(activity_new_mode_white_increment_mine, gameMode.whiteIncrement)

            }
        }
        activity_new_mode_white_increment_plus.setOnClickListener {
            if (gameMode.whiteIncrement < 1000) {
                gameMode.whiteIncrement ++
                binding.invalidateAll()
                checkAllowedIncrement(activity_new_mode_white_increment_mine, gameMode.whiteIncrement)
            }
        }
        activity_new_mode_black_limit_mine.setOnClickListener {
            if (gameMode.blackTime > 1) {
                gameMode.blackTime --
                binding.invalidateAll()
                checkAllowedTime(activity_new_mode_black_limit_mine, gameMode.blackTime)
            }
        }
        activity_new_mode_black_limit_plus.setOnClickListener {
            if (gameMode.blackTime < 1000) {
                gameMode.blackTime ++
                binding.invalidateAll()
                checkAllowedTime(activity_new_mode_black_limit_mine, gameMode.blackTime)
            }
        }
        activity_new_mode_black_increment_mine.setOnClickListener {
            if (gameMode.blackIncrement > 0) {
                gameMode.blackIncrement --
                binding.invalidateAll()
                checkAllowedIncrement(activity_new_mode_black_increment_mine, gameMode.blackIncrement)
            }
        }
        activity_new_mode_black_increment_plus.setOnClickListener {
            if (gameMode.blackIncrement < 1000) {
                gameMode.blackIncrement ++
                binding.invalidateAll()
                checkAllowedIncrement(activity_new_mode_black_increment_mine, gameMode.blackIncrement)
            }
        }
        activity_new_mode_create_button.setOnClickListener {
            createMode()
        }
        activity_new_mode_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!activity_new_mode_name.text.toString().isEmpty()) {
                    creationOk = true
                    activity_new_mode_create_button.setBackgroundColor(Color.parseColor("#104F55"))
                } else {
                    creationOk = false
                    activity_new_mode_create_button.setBackgroundColor(Color.parseColor("#DCDCDC"))
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun checkAllowedTime(button:Button, value: Int) {
        if (value == 1 || value == 1000) {
            button.background = ContextCompat.getDrawable(this, R.drawable.disable_round)
        } else {
            button.background = ContextCompat.getDrawable(this, R.drawable.primary_round)
        }
    }

    private fun checkAllowedIncrement(button:Button, value: Int) {
        if (value == 0 || value == 1000) {
            button.background = ContextCompat.getDrawable(this, R.drawable.disable_round)
        } else {
            button.background = ContextCompat.getDrawable(this, R.drawable.primary_round)
        }
    }

    private fun createMode() {
        if (this.creationOk) {
            if (this.isIdentical) {
                this.gameMode.blackTime = this.gameMode.whiteTime
                this.gameMode.blackIncrement = this.gameMode.whiteIncrement
            }
            gameMode.name = activity_new_mode_name.text.toString()
            gameModeViewModel.insert(gameMode)
            Toast.makeText(this, "'"+gameMode.name + "' mode created", Toast.LENGTH_LONG).show()
            finish()
        } else {
            Toast.makeText(this, "Please choose a name for this game mode", Toast.LENGTH_LONG).show()
        }
    }

    private fun identicalHideOrShow() {
        if (this.isIdentical) {
            activity_new_mode_white_or_both.text = "Both"
            activity_new_mode_black_block.visibility = View.INVISIBLE
        } else {
            activity_new_mode_white_or_both.text = "White"
            activity_new_mode_black_block.visibility = View.VISIBLE
        }
    }
}
