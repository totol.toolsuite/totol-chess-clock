package totol.chessclock.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import totol.chessclock.adapters.GameModeAdapter
import totol.chessclock.callbacks.SwipeToDeleteCallback
import totol.chessclock.entites.GameMode
import totol.chessclock.R
import totol.chessclock.viewmodels.GameModeViewModel
import totol.chessclock.utils.RecyclerItemClickListener

class MainActivity : AppCompatActivity() {

    //Recycler view for the article list
    var recyclerView: RecyclerView? = null

    var modes: MutableList<GameMode> = ArrayList()

    //Adapter for the recycler view of article
    lateinit var adapter: GameModeAdapter

    //View model to access db local
    private lateinit var gameModeViewModel: GameModeViewModel

    val PREFS_FILENAME = "totol.chessclock.prefs"

    var firstime: String = "true"
    var prefs: SharedPreferences? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        gameModeViewModel = ViewModelProviders.of(this).get(GameModeViewModel::class.java)
        checkFirstTimeConnect()

        fabListener()
        gameModeViewModel.getAll().observe(this, Observer { gm ->
            this.modes = gm
            main_activity_title.text = "Game modes ("+ this.modes.size + ")"
            if (modes.size != 0) {
                main_activity_game_mode_list.apply {
                    layoutManager = LinearLayoutManager(context.applicationContext)
                    adapter = GameModeAdapter(gm!!)
                }
            }
        })
        initRecycler()
        enableSwipeToDeleteAndUndoListener()
        setRecyclerListener()
        //enableSwipeToEdit()
    }



    private fun fabListener() {
        main_activity_new_mode_fab.setOnClickListener {
            val newAModeIntent = Intent(this, NewModeActivity::class.java)
            startActivity(newAModeIntent)
        }
    }

    @SuppressLint("WrongConstant")
    private fun initRecycler() {
        recyclerView = main_activity_game_mode_list as RecyclerView

        recyclerView!!.layoutManager =
                LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        adapter = GameModeAdapter(modes)
        recyclerView?.adapter = adapter
    }

    private fun enableSwipeToDeleteAndUndoListener() {
        val swipeToDeleteCallback =
            object : SwipeToDeleteCallback(this.getApplicationContext()!!) {
                override fun onSwiped(@NonNull viewHolder: RecyclerView.ViewHolder, i: Int) {

                    val position = viewHolder.adapterPosition
                    val gm: GameMode = modes[position]

                    val snack = Snackbar.make(
                        findViewById(R.id.main_activity_game_mode_list)!!,
                        "Item was removed from the list.",
                        Snackbar.LENGTH_LONG
                    )
                    gameModeViewModel.deleteOne(gm)
                    snack.setAction("UNDO") {
                        gameModeViewModel.insert(gm)
                        recyclerView?.scrollToPosition(position)
                    }

                    snack.setActionTextColor(Color.YELLOW)
                    snack.show()

                }
            }

        val itemTouchhelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchhelper.attachToRecyclerView(recyclerView)
    }

    private fun setRecyclerListener() {
        main_activity_game_mode_list.addOnItemTouchListener(RecyclerItemClickListener(this.applicationContext!!, main_activity_game_mode_list
            , object : RecyclerItemClickListener.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                //On créer un intent pour passer l'activity TaskDetails
                val intent = Intent(applicationContext, GameActivity::class.java)
                //On rajoute en extra la Task en question à l'aide de sa position dans la liste des taches
                intent.putExtra("extra_white_time", this@MainActivity.modes[position].whiteTime)
                intent.putExtra("extra_white_increment", this@MainActivity.modes[position].whiteIncrement)
                intent.putExtra("extra_black_time", this@MainActivity.modes[position].blackTime)
                intent.putExtra("extra_black_increment", this@MainActivity.modes[position].blackIncrement)
                intent.putExtra("extra_name", this@MainActivity.modes[position].name)
                startActivity(intent)
            }
            override fun onItemLongClick(view: View?, position: Int) {
                //Toast.makeText(this@MainActivity, "Implémenter l'édition ici au moins, et au mieux sur le swipe right", Toast.LENGTH_LONG).show()
            }
        }))
    }

    private fun checkFirstTimeConnect() {
        this.prefs = this.getSharedPreferences(PREFS_FILENAME, 0)
        this.firstime = prefs!!.getString("FIRST_TIME", "true") ?: "true"
        if (this.firstime === "true") {
            val editor = prefs!!.edit()
            editor.putString("FIRST_TIME", "false")
            editor.apply()
            val classic:GameMode = GameMode(0,"Classic",15,15,0,0)
            val blitz:GameMode = GameMode(1,"Blitz",5,5,0,0)
            val bullet:GameMode = GameMode(2,"Bullet delay",1,1,2,2)
            gameModeViewModel.insert(classic)
            gameModeViewModel.insert(blitz)
            gameModeViewModel.insert(bullet)

        }
    }
    /*
    private fun enableSwipeToEdit() {
        val swipeToDeleteCallback =
            object : SwipeToEditCallback(this.getApplicationContext()!!) {
                override fun onSwiped(@NonNull viewHolder: RecyclerView.ViewHolder, i: Int) {

                    val position = viewHolder.adapterPosition
                    val gm: GameMode = modes[position]

                    main_activity_new_mode_fab.setOnClickListener {
                        val newAModeIntent = Intent(baseContext, NewModeActivity::class.java)
                        startActivity(newAModeIntent)
                    }
                }
            }

        val itemTouchhelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchhelper.attachToRecyclerView(recyclerView)
    }*/


}
