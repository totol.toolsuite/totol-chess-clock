package totol.chessclock.activities

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.TypedValue
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.LinearInterpolator
import android.widget.Chronometer
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_game.*
import totol.chessclock.R
import totol.chessclock.databinding.ActivityGameBinding



import totol.chessclock.entites.GameMode

class GameActivity : AppCompatActivity() {

    var gameMode = GameMode()
    var timeStopWhite : Long = 0
    var timeStopBlack : Long = 0
    var paused : Boolean = false
    var buttonClick : AlphaAnimation = AlphaAnimation(1F, 0.8F)

    /*
        Status :
        0 = Still not started
        1 = Other player is playing
        2 = Active
     */
    var whiteStatus : Int = 0
    var blackStatus : Int = 0

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        gameMode.whiteTime = if (intent.extras?.get("extra_white_time") != null) intent.extras?.get("extra_white_time") as Int else 15
        gameMode.whiteIncrement = if (intent.extras?.get("extra_white_increment") != null) intent.extras?.get("extra_white_increment") as Int else 15
        gameMode.blackTime = if (intent.extras?.get("extra_black_time") != null) intent.extras?.get("extra_black_time") as Int else 15
        gameMode.blackIncrement = if (intent.extras?.get("extra_black_increment") != null) intent.extras?.get("extra_black_increment") as Int else 15
        gameMode.name = if (intent.extras?.get("extra_name") != null) intent.extras?.get("extra_name") as String else "Nameless mode"
        val binding : ActivityGameBinding = DataBindingUtil.setContentView(this, R.layout.activity_game)
        binding.mode = gameMode
        setupTime()
        setupListeners()
        setEndGameListener()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setupTime() {
        activity_game_player_1_clock.isCountDown = true
        activity_game_player_1_clock.base = SystemClock.elapsedRealtime() + gameMode.whiteTime * 60000
        activity_game_player_2_clock.isCountDown = true
        activity_game_player_2_clock.base = SystemClock.elapsedRealtime() + gameMode.blackTime * 60000
        timeStopWhite = activity_game_player_1_clock.base - SystemClock.elapsedRealtime()
        timeStopBlack = activity_game_player_2_clock.base - SystemClock.elapsedRealtime()
    }

    private fun setupListeners() {
        activity_game_player_1_clock.setOnClickListener {
            activity_game_player_1_clock.startAnimation(buttonClick)
            animateButton(activity_game_player_1_clock)

            //Si c'est a l'adversaire de jouer le clique est inutile
            if (whiteStatus != 1 && !paused) {
                //Pas le premier clique pour le joueur 1
                //Si c'est le premier coup de la partie, on réajuste la pendule à 0 (il s'est écoulé du temps entre le lancement de la page etle clique)
                if (whiteStatus == 0) activity_game_player_1_clock.base = SystemClock.elapsedRealtime() + gameMode.whiteTime * 60000
                //Si la partie était mise en pause, on réajuste le temps
                //if (paused) activity_game_player_1_clock.base = SystemClock.elapsedRealtime() + timeStopWhite; paused = false
                //On incrémente la pendule 1
                activity_game_player_1_clock.base = activity_game_player_1_clock.base + gameMode.whiteIncrement * 1000
                //On sauvegarde le timing du clique
                timeStopWhite = activity_game_player_1_clock.base - SystemClock.elapsedRealtime()
                //On arrete la pendule 1
                activity_game_player_1_clock.stop()
                //On met à jour la pendule 2
                activity_game_player_2_clock.base = SystemClock.elapsedRealtime() + timeStopBlack
                //On start la pendule 2
                activity_game_player_2_clock.start()
                //On met à jour les 2 status
                whiteStatus = 1
                blackStatus = 2
                setActive(activity_game_player_2_clock)
                setInactive(activity_game_player_1_clock)
            }
        }

        activity_game_player_2_clock.setOnClickListener {
            activity_game_player_2_clock.startAnimation(buttonClick)
            animateButton(activity_game_player_2_clock)
            if (blackStatus != 1 && !paused) {
                if (blackStatus == 0) activity_game_player_2_clock.base = SystemClock.elapsedRealtime() + gameMode.blackTime * 60000
                activity_game_player_2_clock.base = activity_game_player_2_clock.base + gameMode.blackIncrement * 1000
                timeStopBlack = activity_game_player_2_clock.base - SystemClock.elapsedRealtime()
                activity_game_player_2_clock.stop()
                activity_game_player_1_clock.base = SystemClock.elapsedRealtime() + timeStopWhite
                activity_game_player_1_clock.start()
                whiteStatus = 2
                blackStatus = 1
                setActive(activity_game_player_1_clock)
                setInactive(activity_game_player_2_clock)
            }
        }
        activity_game_pause.setOnClickListener {
            animateButton(activity_game_pause)
            if (!paused) {
                if (whiteStatus == 2) {
                    timeStopWhite = activity_game_player_1_clock.base - SystemClock.elapsedRealtime()
                    activity_game_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
                    activity_game_player_1_clock.stop()
                    paused = true
                } else if (blackStatus == 2) {
                    timeStopBlack = activity_game_player_2_clock.base - SystemClock.elapsedRealtime()
                    activity_game_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
                    activity_game_player_2_clock.stop()
                    paused = true
                }
            } else {
                if (whiteStatus == 2) {
                    activity_game_player_1_clock.base = SystemClock.elapsedRealtime() + timeStopWhite
                    activity_game_pause.setImageResource(R.drawable.ic_pause_black_24dp)
                    activity_game_player_1_clock.start()
                    paused = false
                } else if (blackStatus == 2) {
                    activity_game_player_2_clock.base = SystemClock.elapsedRealtime() + timeStopBlack
                    activity_game_pause.setImageResource(R.drawable.ic_pause_black_24dp)
                    activity_game_player_2_clock.start()
                    paused = false
                }
            }
        }

        activity_game_restart.setOnClickListener {
            animateButton(activity_game_restart)
            whiteStatus = 0
            blackStatus = 0
            activity_game_player_1_clock.stop()
            activity_game_player_2_clock.stop()
            activity_game_player_1_clock.base = SystemClock.elapsedRealtime() + gameMode.whiteTime * 60000
            activity_game_player_2_clock.base = SystemClock.elapsedRealtime() + gameMode.blackTime * 60000
        }
        activity_game_quit.setOnClickListener {
            animateButton(activity_game_quit)
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Exit dialog")
            builder.setMessage("Do you really want to quit this game?")
            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                finish()
                Toast.makeText(applicationContext,"Game left", Toast.LENGTH_SHORT).show()
            }

            builder.setNegativeButton(android.R.string.no) { dialog, which ->
                Toast.makeText(applicationContext,"Game resumed", Toast.LENGTH_SHORT).show()
            }

            builder.show()
        }
    }
    @SuppressLint("SetTextI18n")
    private fun setEndGameListener() {
        activity_game_player_1_clock.setOnChronometerTickListener {
            if (activity_game_player_1_clock.text.toString() == "00:00" ||
                activity_game_player_1_clock.text.toString()[0].toString() == "-") {
                activity_game_player_1_clock.text = "00:00"
                Toast.makeText(this, "GG", Toast.LENGTH_LONG).show()
                endGame()
            }
        }
        activity_game_player_2_clock.setOnChronometerTickListener {
            if (activity_game_player_2_clock.text.toString() == "00:00" ||
                activity_game_player_2_clock.text.toString()[0].toString() == "-"
            ) {
                activity_game_player_2_clock.text = "00:00"
                Toast.makeText(this, "GG", Toast.LENGTH_LONG).show()
                endGame()
            }
        }
    }

    private fun endGame() {
        activity_game_player_1_clock.stop()
        activity_game_player_2_clock.stop()
        whiteStatus = 1
        blackStatus = 1
    }

    private fun setActive(chronometer: Chronometer) {
        chronometer.setTextSize(TypedValue.COMPLEX_UNIT_SP, 100F)
        chronometer.setElevation(0F)
        chronometer.setPadding(TypedValue.COMPLEX_UNIT_SP, 210,0,0)
        chronometer.setBackgroundColor(Color.parseColor("#ED3663"))
    }
    private fun setInactive(chronometer: Chronometer) {
        chronometer.setTextSize(TypedValue.COMPLEX_UNIT_SP, 80F)
        chronometer.setElevation(10F)
        chronometer.setPadding(TypedValue.COMPLEX_UNIT_SP, 240,0,0)
        chronometer.setBackgroundColor(Color.parseColor("#14281D"))
    }

    private fun animateButton(chronometer: View) {
        chronometer.alpha = 0.2f
        chronometer.animate().apply {
            interpolator = LinearInterpolator()
            duration = 50
            alpha(1f)
            startDelay = 100
            start()
        }
    }
}
