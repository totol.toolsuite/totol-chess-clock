package totol.chessclock.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import totol.chessclock.entites.GameMode
import totol.chessclock.R

class GameModeAdapter(private val modeList: List<GameMode>) : RecyclerView.Adapter<GameModeViewHolder>() {
    override fun onBindViewHolder(holder: GameModeViewHolder, position: Int) {
        val gm: GameMode = modeList[position]
        holder.bind(gm)
    }

    override fun getItemCount(): Int {
        return modeList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, modeId: Int): GameModeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return GameModeViewHolder(inflater, parent)
    }
}
class GameModeViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.game_mode_card, parent, false)) {
    var whiteTime = itemView.findViewById(R.id.game_mode_card_white_time) as TextView
    var blackTime = itemView.findViewById(R.id.game_mode_card_black_time) as TextView
    var whiteIncrement = itemView.findViewById(R.id.game_mode_card_white_increment) as TextView
    var blackIncrement = itemView.findViewById(R.id.game_mode_card_black_increment) as TextView
    var name = itemView.findViewById(R.id.game_mode_card_name) as TextView

    @SuppressLint("SetTextI18n")
    fun bind(gameMode: GameMode) {
        whiteTime.text = gameMode.whiteTime.toString()+":00"
        blackTime.text = gameMode.blackTime.toString()+":00"
        if (gameMode.whiteIncrement > 0)
            whiteIncrement.text = "+"+gameMode.whiteIncrement.toString()
        else whiteIncrement.text = gameMode.whiteIncrement.toString()
        if (gameMode.blackIncrement > 0)
        blackIncrement.text = "+"+gameMode.blackIncrement.toString()
        else blackIncrement.text = "+"+gameMode.blackIncrement.toString()
        name.text = gameMode.name
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())
        }
        return this
    }
}