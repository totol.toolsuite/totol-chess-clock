package totol.chessclock.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import totol.chessclock.entites.GameMode

@Dao
interface GameModeDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(gameMode: GameMode)

    @Query("SELECT * FROM game_mode")
    fun getAll(): LiveData<MutableList<GameMode>>

    @Query("SELECT * FROM game_mode WHERE id = :id")
    fun getById(id: Int) : LiveData<GameMode>

    @Delete
    suspend fun deleteOne(gameMode: GameMode)

    @Update
    suspend fun update(gameMode: GameMode)

    @Query("DELETE FROM game_mode")
    fun nukeTable()
}