package totol.chessclock.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import totol.chessclock.daos.GameModeDao
import totol.chessclock.entites.GameMode

@Database(entities = arrayOf(GameMode::class), version = 1, exportSchema = true)
abstract class AppDB : RoomDatabase()
{
    abstract fun gameModeDao() : GameModeDao

    companion object {
        @Volatile
        private var INSTANCE: AppDB? = null

        fun getDatabase(context: Context): AppDB {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDB::class.java,
                        "app_database"
                ).fallbackToDestructiveMigration()
                        .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}